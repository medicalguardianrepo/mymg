# README

## My MG
A framework for allowing companion users to interact with devices and care circle members.

### How do I get set up?
* Clone the repository - ```git clone git@bitbucket.org:medicalguardianrepo/mymg.git```
* Install cordova as system dependency - ```npm install -g cordova```
* Install dependencies - ```npm install``` (at project root)
* Add Platforms:
    * Add Android platform codebase - ```cordova platform add android```
    * Add Browser platform codebase - ```cordova platform add browser```
    * Add iOS platform codebase - ```cordova platform add ios```
* Build for release:
    * Build all release packages - ```cordova build```
    * Build Android release package - ```cordova build android```
    * Build Browser release package - ```cordova build browser```
    * Build iOS release package - ```cordova build ios```
* Run for Development (in browser):
    * Run development code in Browser - ```cordova run browser```
* Emulate for Development:
    * Run development code in Android emulator (Windows only) - ```cordova emulate android```
    * Run development code in iOS emulator (Mac only) - ```cordova emulate ios```

### Who do I talk to?
* Steve D. <steve.denier@medicalguardian.com>