import React, { Component } from "react";

class LoadingSpinnerView extends Component {
  constructor (props) {
      super(props);
      this.state = {};
  }
  render() {
    return (
        <div id="loadingSpinner" className="lds-roller">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    );
  }
}

export default LoadingSpinnerView;