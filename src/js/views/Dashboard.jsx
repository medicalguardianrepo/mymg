import React, { Component } from "react";
import { Redirect } from 'react-router-dom';
import isLoggedIn from '../helpers/loggedIn.jsx';
import LocationView from './Location.jsx';

class Dashboard extends Component {
  render() {
    if (!isLoggedIn()) {
      return <Redirect to="/login" />;
    }

    return (
      <div>
        <div id="dashboardNav">
          <ul>
            <li className="active">Location</li>
            <li>Watch Profile</li>
          </ul>
        </div>
        <div id="dashboardContent">
          <LocationView />
        </div>
      </div>
    );
  }
}

export default Dashboard;