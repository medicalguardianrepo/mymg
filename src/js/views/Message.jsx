import React, { Component } from "react";

class MessageView extends Component {
  constructor (message) {
      super(message);
      this.state = {message: message};
  }
  render() {
    return (
      <div className="messageBubble">
          <div className="timestamp"></div>
          <div className="message">{this.state.message}</div>
      </div>
    );
  }
}

export default MessageView;