import React, { Component } from "react";
import store from 'store';

class Logout extends Component {
  render() {

    store.remove('loggedIn');
    console.log('you have logged out...');

    return (
      <div>
        <p>You have been logged out.</p>
      </div>
    );
  }
}

export default Logout;