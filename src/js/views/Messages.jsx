import React, { Component } from "react"
import ReactDOM from 'react-dom';
import axios from 'axios';
import store from 'store';
import MessageView from "./Message.jsx";

class Messages extends Component {

    constructor(props) {
        super(props);
        this.state = { inputMessage: '', messages: [], userName: '' };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.sendMessage = this.sendMessage.bind(this);
        this.getMessages = this.getMessages.bind(this);

        this.messageIDs = [];

        this.getMessages();
        this.poll = setInterval(this.getMessages, 5000);

    }

    componentWillUnmount() {
        clearInterval(this.poll);
    }

    handleInputChange(e) {
        this.setState({ inputMessage: e.target.value });
    }

    getMessage(message_id) {

        // const token = localStorage.getItem('token');
        const token = store.get('token');

        let messageBody = { "info": { "token": token }, "data": { "msg_id": message_id } };

        let self = this;

        axios({
            method: 'POST',
            url: 'http://mgiot.localhost/v2/device/getMessage/123456789',
            data: messageBody,
            headers: {
                "bypass_encryption": true
            }
        })
        .then(function (response) {
            const messages = self.state.messages;
            let message = response.data.data;

            messages.push({
                // TODO need to pass from ID
                userName: "Mary",
                text: message
            });

            self.setState({ messages: messages });
        })
        .catch(function (error) {
            console.log(error);
        });
    }

    getMessages(message_id) {

        // const token = localStorage.getItem('token');
        const token = store.get('token');

        let messageBody = { "info": { "token": token }, "data": { "user_id": 1, "watch_id": 2 } };

        let self = this;

        axios({
            method: 'POST',
            url: 'http://mgiot.localhost/v2/device/getMessages/123456789/true',
            data: messageBody,
            headers: {
                "bypass_encryption": true
            }
        })
            .then(function (response) {
                const messages = self.state.messages;
                const fetchedMessages = response.data.data;

                fetchedMessages.forEach(fetchedMessage => {
                    if (self.messageIDs && self.messageIDs.length) {
                        if (self.messageIDs.indexOf(fetchedMessage.msg_id) <= -1) {
                            messages.push({
                                id: fetchedMessage.msg_id,
                                expired_at: fetchedMessage.expiry_date,
                                created_at: fetchedMessage.created_at,
                                from_id: fetchedMessage.s_id,
                                userName: fetchedMessage.s_name,
                                text: fetchedMessage.payload
                            });

                            self.messageIDs.push(fetchedMessage.msg_id);

                            self.setState({ messages: messages });
                            self.scrollFix();
                        }
                    } else {
                        messages.push({
                            id: fetchedMessage.msg_id,
                            expired_at: fetchedMessage.expiry_date,
                            created_at: fetchedMessage.created_at,
                            from_id: fetchedMessage.s_id,
                            userName: fetchedMessage.s_name,
                            text: fetchedMessage.payload
                        });

                        self.messageIDs.push(fetchedMessage.msg_id);

                        self.setState({ messages: messages });
                        self.scrollFix();
                    }
                })
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    scrollFix () {
        let scrollContainer = document.getElementById("messages");
        scrollContainer.scrollTop = scrollContainer.scrollHeight;
    }

    sendMessage() {
        // const token = localStorage.getItem('token');
        const token = store.get('token');

        let messageBody = { "info": { "token": token }, "data": { "parent_msg_id": "2", "user_id": "1", "msg_type": "text" } }

        if (this.state) {
            messageBody.data.payload = this.state.inputMessage;
        } else {
            return false;
        }

        let self = this;

        axios({
            method: 'POST',
            url: 'http://mgiot.localhost/v2/device/sendMessage/123456789',
            data: messageBody,
            headers: {
                "bypass_encryption": true
            }
        })
            .then(function (response) {

                console.log(response.data.data);



                const messages = self.state.messages;

                messages.push({
                    userName: response.data.data.n_msg[0].user_name,
                    text: self.state.inputMessage
                });

                self.setState({ messages: messages });
                self.scrollFix();
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    render() {
        return (
            <div>
                <div id="messages">
                    <ul className="Messages-list">
                        {this.state.messages.map((m, i) => this.renderMessage(m, i))}
                    </ul>
                </div>
                <div id="sendMessage">
                    <div id="messageInputContainer">
                        <input type="text" name="messageInput" id="messageInput" placeholder="Enter your message" value={this.state.value} onChange={this.handleInputChange}></input>
                    </div>
                    <div id="messageBtn" onClick={this.sendMessage}><i className="fa fa-send fa-2x"></i></div>
                </div>
            </div>
        );
    }

    renderMessage(message, i) {
        const { id, expired_at, created_at, from_id, userName, text } = message;
        const user_id = 1; // hard-coded to appear as John using the companion app
        let fromId;
        let d = new Date();
        let currentTime = d.getFullYear()+"-"+(d.getMonth() +1)+"-"+d.getDate()+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();
        let timestamp = typeof created_at === 'undefined' ? currentTime : created_at;

        if (typeof from_id === 'undefined') {
            fromId = 1; // hard-coded to appear as Jogn using the companion app
        } else {
            fromId = from_id;
        }
        
        const className = fromId === user_id ? "message currentUser" : "message";

        // TODO check expiry date before rendering or should server check
        return (
            <li key={i} className={className} id={id}>
                <div className="Message-content">
                    <div className="username">{userName} {timestamp}</div>
                    <div className="text">{text}</div>
                </div>
            </li>
        );
    }
}

export default Messages;