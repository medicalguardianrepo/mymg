import store from 'store'
import React, { Component } from "react";

class Login extends Component {
  constructor (props) {
    super(props);

    this.state = {
      username: '',
      password: '',
      error: false
    };

    this.handleUsernameChange = this.handleUsernameChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit (e) {
    e.preventDefault();

    const { username, password } = this.state;
    const { history } = this.props;

    this.setState({ error: false });

    if (!(username === 'john' && password === 'test')) {
      return this.setState({error: true});
    }

    console.log('login successful');
    store.set('loggedIn', true);
    history.push('/dashboard');
  }

  handleUsernameChange(e) {
    this.setState({ "username": e.target.value });
  }

  handlePasswordChange(e) {
    this.setState({ "password": e.target.value });
  }

  render() {

    const {error} = this.state;

    // document.getElementById('header').style.visibility = "hidden";
    // document.getElementById('sidenav').style.visibility = "hidden";
    
    return (
      <div id="LoginView">
        <div id="loginLogo"></div>
        <form id="loginForm" onSubmit={ this.onSubmit }>
          <input name="username" type="text" label="Username" placeholder="Enter Your Username" onChange={this.handleUsernameChange}/>
          <input name="password" type="password" label="Password" placeholder="Enter Your Password" onChange={this.handlePasswordChange}/>
          <button>Login</button>
        </form>
      </div>
    );
  }
}

export default Login;