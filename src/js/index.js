import React from "react";
import ReactDOM from "react-dom"
import axios from 'axios'
import store from 'store'
import App from './controllers/App.jsx'

var bootloader = {
  // Application Constructor
  initialize: function () {
    document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    this.auth();
  },

  auth: function () {
    axios({
      method: 'POST',
      url: 'http://mgiot.localhost/v2/device/auth',
      data: '{"key":"123e4567-e89b-12d3-a456-426655440000","dc":"test_class","did":"123456789","app":3,"ver":"13"}',
      headers: {
        "bypass_encryption": true
      },
      mode: 'no-cors',
    })
    .then(function (response) {        
      // localStorage.setItem('token', response.data.data.token);        
      store.set('token', response.data.data.token);
    })
    .catch(function (error) {
      console.log(error);
    });
  },

  // deviceready Event Handler
  //
  // Bind any cordova events here. Common events are:
  // 'pause', 'resume', etc.
  onDeviceReady: function () {
    ReactDOM.render(
      <App />,
      document.getElementById("app")
    );
  },
};

bootloader.initialize();