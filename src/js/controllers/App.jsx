
import React, { Component } from "react"
import { Route, NavLink, HashRouter } from "react-router-dom"
import LoginTitle from "../views/LoginTitle.jsx"
import DashboardTitle from "../views/DashboardTitle.jsx"
import AlertsTitle from "../views/AlertsTitle.jsx"
import ActivityTitle from "../views/ActivityTitle.jsx"
import CareCircleTitle from "../views/CareCircleTitle.jsx"
import MessagesTitle from "../views/MessagesTitle.jsx"
import CalendarTitle from "../views/CalendarTitle.jsx"
import AboutUsTitle from "../views/AboutUsTitle.jsx"
import LogoutTitle from "../views/LogoutTitle.jsx"
import LoginView from "../views/Login.jsx"
import DashboardView from "../views/Dashboard.jsx"
import AlertsView from "../views/Alerts.jsx"
import ActivityView from "../views/Activity.jsx"
import CareCircleView from "../views/CareCircle.jsx"
import MessagesView from "../views/Messages.jsx"
import CalendarView from "../views/Calendar.jsx"
import AboutUsView from "../views/AboutUs.jsx"
import LogoutView from "../views/Logout.jsx"
import "font-awesome/css/font-awesome.min.css"

class App extends Component {

  render() {
    return (
      <HashRouter>
        <div>
          <div id="header">
            <span id="menuBtn" onClick={this.openNav}><i className="fa fa-bars fa-2x"></i></span>
            <div id="headerTitle">
              <Route path="/login" component={LoginTitle} />
              <Route path="/dashboard" component={DashboardTitle} />
              <Route path="/alerts" component={AlertsTitle} />
              <Route path="/activity" component={ActivityTitle} />
              <Route path="/care-circle" component={CareCircleTitle} />
              <Route path="/messages" component={MessagesTitle} />
              <Route path="/calendar" component={CalendarTitle} />
              <Route path="/about-us" component={AboutUsTitle} />
              <Route path="/logout" component={LogoutTitle} />
            </div>
          </div>
          <div id="sidenav">
            <div id="sidenav-header">
              <div id="sidenav-logo"></div>
              <div id="sidenav-header-inner">
                <div id="sidenav-avatar">
                  <span className="fa-stack fa-2x">
                    <i className="fa fa-circle fa-stack-2x"></i>
                    <i className="fa fa-user fa-stack-1x fa-inverse"></i>
                  </span>
                  <p>Test User</p>
                </div>
                <div id="sidenav-divider"></div>
                <div id="sidenav-device">
                  <span className="fa-stack fa-2x">
                    <i className="fa fa-circle fa-stack-2x"></i>
                    <i className="fa fa-tablet fa-stack-1x fa-inverse"></i>
                  </span>
                  <p>0XM8924</p>
                </div>
              </div>
              {/* <span id="menuCloseBtn" onClick={this.closeNav}><i className="fa fa-close"></i></span> */}
            </div>
            <div id="sidenav-body">
              <ul>
                <li><NavLink to="/login" onClick={this.closeNav}>Login</NavLink></li>
                <li><NavLink to="/dashboard" onClick={this.closeNav}>Dashboard</NavLink></li>
                <li><NavLink to="/alerts" onClick={this.closeNav}>Alerts</NavLink></li>
                <li><NavLink to="/activity" onClick={this.closeNav}>Activity</NavLink></li>
                <li><NavLink to="/care-circle" onClick={this.closeNav}>Care Circle</NavLink></li>
                <li><NavLink to="/messages" onClick={this.closeNav}>Messages</NavLink></li>
                <li><NavLink to="/calendar" onClick={this.closeNav}>Calendar</NavLink></li>
                <li><NavLink to="/about-us" onClick={this.closeNav}>About Us</NavLink></li>
                <li><NavLink to="/logout" onClick={this.closeNav}>Logout</NavLink></li>
              </ul>
            </div>
          </div>
          <div id="content">
            <Route path="/login" component={LoginView} />
            <Route path="/dashboard" component={DashboardView} />
            <Route path="/alerts" component={AlertsView} />
            <Route path="/activity" component={ActivityView} />
            <Route path="/care-circle" component={CareCircleView} />
            <Route path="/messages" component={MessagesView} />
            <Route path="/calendar" component={CalendarView} />
            <Route path="/about-us" component={AboutUsView} />
            <Route path="/logout" component={LogoutView} />
          </div>
        </div>
      </HashRouter>
    );
  }

  openNav() {
    document.getElementById("sidenav").style.width = "350px";
    document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
  }

  closeNav() {
    document.getElementById("sidenav").style.width = "0";
    document.body.style.backgroundColor = "white";
  }
}

export default App;